const loggerUtil = require('@jht/logger-util'),
    mongodb_client = require('mongodb').MongoClient,
    deasync = require('deasync');

module.exports = initializeConfig();

/**
 * @description creates and returns a brand new config object. Also sets
 * the global config for the given environment for caching
 * @parameter {string} - env - Specifies which environment you want the config
 * object to be for.
 * @returns {object} - completely new config object
 */
function initializeConfig() {
    const config = createConfig();
    setupLogger(config);
    return createPools(config);
}

function createConfig() {
    const config = {
        corsAllowedOrigins: process.env.CORS_ALLOWED_ORIGINS,
        dapiEnvironment: process.env.DAPI_ENVIRONMENT,
        dapiGateway: process.env.DAPI_GATEWAY,
        dapiServerUrl: process.env.DAPI_SERVER_URL,
        dapiTotpSecret: process.env.DAPI_TOTP_SECRET,
        jwtSecretKey: process.env.JWT_SECRET_KEY,
        jwtnTotpSecret: process.env.JWTN_TOTP_SECRET,
        localLogOnly: process.env.LOCAL_LOG_ONLY || false,
        logLevel: process.env.LOG_LEVEL || 'info',
        logglyInputToken: process.env.LOGGLY_INPUT_TOKEN,
        logglySubdomain: process.env.LOGGLY_SUBDOMAIN,
        logglyTag: process.env.LOGGLY_TAG,
        machineCacheApiKey: process.env.MACHINE_CACHE_API_KEY,
        mongoExerciser: process.env.MONGO_EXERCISER,
        mongoMachineCache: process.env.MONGO_MACHINE_CACHE,
        nodeEnv: process.env.NODE_ENV,
    };

    [
        'corsAllowedOrigins',
        'dapiEnvironment',
        'dapiGateway',
        'dapiServerUrl',
        'dapiTotpSecret',
        'jwtSecretKey',
        'jwtnTotpSecret',
        'localLogOnly',
        'logLevel',
        'logglyInputToken',
        'logglySubdomain',
        'logglyTag',
        'machineCacheApiKey',
        'mongoExerciser',
        'mongoMachineCache',
        'nodeEnv'
    ].forEach(function(requiredVal) {
        if (config[requiredVal] === undefined) {
            throw new Error(`No ${requiredVal} in environment`);
        }
    });

    return config;
}

function setupLogger(config) {
    const localLogOnly = config.localLogOnly === 'true';

    loggerUtil.setupLogger(config.logLevel, localLogOnly, {
        logglySubdomain: config.logglySubdomain,
        logglyToken: config.logglyInputToken,
        tags: [ config.logglyTag ]
    });
}

/**
 * Create db pools
 */
function createPools(config) {

    let exerciserDb,
        machineCacheDb;

    const logger = loggerUtil.getLogger();

    mongodb_client.connect(config.mongoExerciser, { useUnifiedTopology: true }, function(err, eConn) {
        if (!err) {

            exerciserDb = eConn;

            logger.info('Exerciser Database Connected');
        }
    });

    deasync.loopWhile(function() {
        return !exerciserDb;
    });

    mongodb_client.connect(config.mongoMachineCache, { useUnifiedTopology: true }, function(err, mcConn) {
        if (!err) {
            machineCacheDb = mcConn;

            logger.info('Machine Cache Database Connected');
        }
    });

    deasync.loopWhile(function() {
        return !machineCacheDb;
    });

    const close = async function() {
        await exerciserDb.close();
        await machineCacheDb.close();
    }

    config.pools = {
        exerciserDb: exerciserDb.db(),
        machineCacheDb: machineCacheDb.db(),
        close
    };

    return config;
}
