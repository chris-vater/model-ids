const Promise = require('bluebird'),
    request = require('request'),
    _ = require('underscore'),
    moment = require('moment'),
    log = require('@jht/logger-util').getLogger(),
    basicModelCache = {},
    cacheKeyIndex = {},
    ttlInDays = 2;

module.exports = function(config) {
    function _invalidTupleValue(val) {
        return val === null || val === "";
    }

    function _getTupleCacheKey(tuple) {
        if (_invalidTupleValue(tuple.brand_id) || _invalidTupleValue(tuple.machine_type_id) || _invalidTupleValue(tuple.model_type_id) || _invalidTupleValue(tuple.console_type_id)) {
            // do not cache if empty
            return;
        }

        return [
            tuple.brand_id.toString(),
            tuple.machine_type_id.toString(),
            tuple.model_type_id.toString(),
            tuple.console_type_id.toString()
        ].join('_');
    }

    function _getSerialCacheKey(serial) {
        if (!serial.frame_serial_number || !serial.console_serial_number) {
            // do not cache if empty
            return;
        }

        return [
            serial.frame_serial_number.toString(),
            serial.console_serial_number.toString()
        ].join('_');
    }

    function _cacheModel(body, cacheKey) {
        if (!cacheKey) {
            return;
        }

        const modelId = body._id;

        if (_.isUndefined(basicModelCache[modelId])) {
            basicModelCache[modelId] = {};
        }

        cacheKeyIndex[cacheKey] = modelId;
        basicModelCache[modelId].model = body;
        basicModelCache[modelId].lastUpdated = moment();
    }

    const getById = function(modelId) {
        if (
            basicModelCache[modelId] &&
            basicModelCache[modelId].lastUpdated.isSameOrAfter(moment().subtract(ttlInDays, 'days'))
        ) {
            return Promise.resolve(basicModelCache[modelId].model);
        }

        return new Promise(function(resolve, reject) {
            const requestOpts = {
                method: 'GET',
                url: `${config.dapiGateway}/machine/model/${modelId}`,
                json: true
            };

            request(requestOpts, function(err, resp, body) {
                if (err || resp.statusCode !== 200) {
                    log.error('model-getById-error', {
                        requestOpts,
                        err
                    });
                    reject();
                } else {
                    _cacheModel(body, _getTupleCacheKey(body.tuple));
                    resolve(body);
                }
            });
        });
    };

    const getByTuple = function(brand_id, machine_type_id, model_type_id, console_type_id) {
        const cacheKey = _getTupleCacheKey({ brand_id, machine_type_id, model_type_id, console_type_id }),
            url = `${config.dapiGateway}/machine/models/resolvers/ids`,
            body = {
                brand_id: brand_id.toString(),
                machine_type: machine_type_id.toString(),
                machine_console_model_type_id: model_type_id.toString(),
                machine_console_type_id: console_type_id.toString()
            };

        return _machineLookup(cacheKey, url, body);
    };

    const getBySerial = function(frame_serial_number, console_serial_number) {
        const cacheKey = _getSerialCacheKey({ frame_serial_number, console_serial_number }),
            url = `${config.dapiGateway}/machine/models/resolvers/serial-numbers`,
            body = {
                frame_serial_number: frame_serial_number.toString(),
                console_serial_number: console_serial_number.toString()
            };
        return _machineLookup(cacheKey, url, body);
    };

    const _machineLookup = function(cacheKey, url, body) {
        if (
            cacheKey &&
            cacheKeyIndex[cacheKey] &&
            basicModelCache[cacheKeyIndex[cacheKey]] &&
            basicModelCache[cacheKeyIndex[cacheKey]].lastUpdated.isSameOrAfter(moment().subtract(ttlInDays, 'days'))
        ) {
            return Promise.resolve(basicModelCache[cacheKeyIndex[cacheKey]].model);
        }

        return new Promise(function(resolve, reject) {
            const requestOpts = {
                method: 'POST',
                url,
                body,
                json: true
            };

            request(requestOpts, function(err, resp, body) {
                if (err || resp.statusCode !== 200) {
                    log.error('model-machine-lookup-error', {
                        requestOpts,
                        err
                    });
                    reject();
                } else {
                    _cacheModel(body, cacheKey);
                    resolve(body);
                }
            });
        });
    };

    return {
        getById,
        getByTuple,
        getBySerial
    };
}
