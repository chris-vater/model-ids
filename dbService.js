const ObjectID = require('mongodb').ObjectId,
    modelServiceFactory = require('./model.js'),
    _ = require('underscore'),
    Promise = require('bluebird'),
    request = Promise.promisify(require('request')),
    log = require('@jht/logger-util').getLogger();

module.exports = function(config) {
    const db = config.pools.exerciserDb,
        machineCacheDb = config.pools.machineCacheDb,
        modelService = modelServiceFactory(config),
        models = {},
        machineModels = {};

    async function bulkLookupByWorkouts(workouts) {
        const ids = _getIdsToLookup(workouts);

        await _lookupModelsByMachineId(ids.machine);
    }

    async function fetchWorkouts(lastRecord, skip, limit) {
        try {
            const results = await db.collection('workout').find(
                {
                    model_id: {
                        $exists: false
                    },
                    _id: {
                        $gt: ObjectID(lastRecord)
                    }
                }
            ).sort(
                {
                    _id: 1
                }
            ).skip(skip).limit(limit);
    
            return results.toArray();
        } catch (err) {
            const error = JSON.stringify(err, Object.getOwnPropertyNames(err))
            log.error('find-workouts-wo-model-id-error', {
                error
            });
        }

        return false;
    }

    async function augmentWorkoutWithModelData(workout) {
        let modelId = null, model, exerciseTitle = 'unknown', machineType = 'unknown', dirty = false;
        if (workout.model_id) {
            modelId = workout.model_id;
        } else if (workout.machine_id && machineModels[workout.machine_id]) {
            modelId = machineModels[workout.machine_id];
        }

        if (modelId && models[modelId]) {
            model = models[modelId];
            exerciseTitle = model.machine_type_code || 'unknown';
            machineType = model.modality || 'unknown';
            
            if (machineType != 'unknown' && (!workout.model_id || workout.machine_type !== machineType)) {
                dirty = true;
            }
        }

        workout.exercise_title = exerciseTitle;
        workout.machine_type = machineType;
        if (modelId) {
            workout.model_id = modelId;
        }

        if (dirty) {
            await _updateWorkout(workout);
            console.log('updated workout', _.pick(workout, '_id', 'machine_type', 'workout_time', 'machine_id', 'model_id'));
            return 0;
        } else {
            console.log('skipped workout', _.pick(workout, '_id', 'machine_type', 'exercise_title', 'machine_id'));
            return 1;
        }
    }

    function _getIdsToLookup(workouts) {
        const ids = workouts.reduce((ids, workout) => {
            if (workout.machine_id) {
                ids.machine[workout.machine_id] = workout.machine_id;
            }

            return ids;
        }, { machine: {} });

        return {
            machine: Object.keys(ids.machine)
        };
    }

    // currently setup to send a request in a loop,
    // may need to update this for sending a batch in a single request
    // (but that will require an additional change on the API's side)
    async function _getMachineInfoFromDapi(machineIds) {
        try {
            return (
                await Promise.all(machineIds.map(async (machineId) => {
                    const opts = {
                        url: config.dapiGateway + '/machine/cache',
                        method: 'GET',
                        qs: {
                            key: 'id',
                            value: machineId
                        },
                        headers: {
                            'x-api-key': config.machineCacheApiKey
                        },
                        json: true
                    };

                    try {
                        const response = await request(opts);

                        if (response.statusCode === 200) {
                            return response.body;
                        }

                        return {};
                    } catch (err) {
                        return {};
                    }
                }))
            ).filter((machine) => !!machine.id);
        } catch (err) {
            return [];
        }
    }

    async function _getMachineInfoFromDb(machineIds) {
        try {
            return await machineCacheDb.collection('machine').find({
                _id: {
                    $in: machineIds
                }
            }).toArray();
        } catch (err) {
            return [];
        }
    }

    async function getMachineInfo(machineIds) {
        // first get any machine data from the DAPI endpoint
        let machines = await _getMachineInfoFromDapi(machineIds);
        // now check the db for any lingering machines which are missing data and merge
        const unknownMachines = _.difference(machineIds, _.pluck(machines, 'id'));
        machines = machines.concat(await _getMachineInfoFromDb(unknownMachines));

        return machines.map((machine) => {
            if (machine._id) {
                machine.id = machine._id;
            }

            return machine;
        });
    }

    async function _lookupModelsByMachineId(machineIds) {
        let machines, model;

        machines = await getMachineInfo(machineIds);

        for (const machine of machines) {
		model = false;

            if (machine.brand_id && machine.machine_type_id && machine.model_type_id && machine.console_type_id) {
                try {
                    model = await modelService.getByTuple(
                        machine.brand_id,
                        machine.machine_type_id,
                        machine.model_type_id,
                        machine.console_type_id
                    );
                } catch (err) {}
            }

            if (!model && machine.frame_serial_number && machine.console_serial_number) {
                try {
                    model = await modelService.getBySerial(
                        machine.frame_serial_number,
                        machine.console_serial_number
                    );
                } catch (err) {}
            }

            if (model) {
                models[model._id] = model;
                machineModels[machine.id] = model._id;
            }
        }
    }

    async function _lookupModelsById(modelIds) {
        let model;

        for (const modelId of modelIds) {
            try {
                model = await modelService.getById(modelId);
                models[model._id] = model;
            } catch (err) {}
        }
    }

    async function _updateWorkout(workout) {
        try {
            await db.collection('workout').updateOne(
                { _id: ObjectID(workout.id) },
                {
                    $set: {
                        machine_type: workout.machine_type,
                        model_id: workout.model_id
                    }
                }
            );
        } catch (err) {
            log.error('update-workout-error', {
                workout,
                err
            });
        }
    }

    return {
        fetchWorkouts,
        bulkLookupByWorkouts,
        augmentWorkoutWithModelData,
        getMachineInfo
    };
};
