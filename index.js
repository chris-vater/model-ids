global.nodeEnv = 'local';
global.workoutFetchLimit = 10;
global.recordsToProcess = 100;
global.errorFile = './error.log';
global.pidFile = './.pid';
const fs = require('fs');

async function initEnvVars() {
    require('dotenv').config();
    return process.env;
}

async function main() {
    const start = Date.now();

    if (fs.existsSync(pidFile)) {
        console.log('Pid file exists, skipping run');
        return;
    }

    if (fs.existsSync(errorFile)) {
        console.log('Error file exists, skipping run');
        return;
    }

    fs.writeFileSync(pidFile, process.pid);

    await initEnvVars();
    
    const DBRef = require('mongodb').DBRef,
        ObjectID = require('mongodb').ObjectId,
        _ = require('underscore'),
        Promise = require('bluebird'),
        config = require('./config.js'),
        dbServiceFactory = require('./dbService'),
        dbService = dbServiceFactory(config),
        lastRecordFile = './lastRecord.txt';

    let skip,
        proccessedIds,
        recordsProcessed = 0,
        recordsToSkip = 0,
        lastRecord,
        lastProccessedId;

    try {
        lastRecord = fs.readFileSync(lastRecordFile, 'utf8').trim();
    } catch(e) {
        console.error('Unable to read lastRecordFile');
        return;
    };

    while(recordsProcessed < recordsToProcess) {
        try {
            // fetch x number of workouts
            const workouts = await dbService.fetchWorkouts(lastRecord, recordsToSkip, workoutFetchLimit);

            const toProcessIds = workouts.map((workout) => {
                return _.pick(workout, '_id');
            });

            await dbService.bulkLookupByWorkouts(workouts);

            for (const workout of workouts) {
                workout.id = workout.id || workout._id; // the fuck?
                lastProccessedId = workout.id;

                // returns 0 if successful, 1 if we need to now skip it
                recordsToSkip += await dbService.augmentWorkoutWithModelData(workout);
            }

            recordsProcessed += workoutFetchLimit;
        } catch(error) {
            const err = JSON.stringify(error, Object.getOwnPropertyNames(error));
            fs.writeFileSync(errorFile, JSON.stringify({ 
                err,
                lastRecord
            }));
            process.exit(1);
        }
    }

    if (lastProccessedId) {
        fs.writeFileSync(lastRecordFile, lastProccessedId);
    } else {
        fs.writeFileSync(errorFile, 'lastProccessedId not set');
    }
    
    const millisElapsed = Date.now() - start;
    console.log(`secondsElapsed: ${Math.floor(millisElapsed / 1000)}`);
    console.log(`recordsProcessed: ${recordsProcessed}`);
    console.log(`recordsSkipped: ${recordsToSkip}`);
    console.log(`lastRecord: ${lastProccessedId}`);

    return config;
}

main().then((config) => {
    // close connections
    if(typeof config !== 'undefined') {
        config.pools.close();
    }

    try {
        pidInFile = parseInt(fs.readFileSync(pidFile, 'utf8'));
        if(pidInFile === process.pid) {
            fs.unlinkSync(pidFile);
        } else {
            console.log('Previous job still in progress, skipping');
        }
    } catch (error) {
        fs.writeFileSync(errorFile, JSON.stringify({ 
            error,
            lastRecord
        }));
        process.exit(1);
    }

    console.log('done');
}).catch((error) => {
    fs.writeFileSync(errorFile, JSON.stringify({ 
        error,
        lastRecord
    }));
});
